print('1:RGB->CMY  2:CMY->RGB  3:RGB->HSL  4:HSL->RGB ')
choice = input('Enter the number of the conversion you want to do: ')


if choice == '1':
    print()
    red = int(input("value of red(0-255): "))/255
    green = int(input("value of green(0-255): "))/255
    blue = int(input("value of blue(0-255): "))/255

    cyan = round((1-red)*100)
    magenta = round((1-green)*100)
    yellow = round((1-blue)*100)

    print()
    print("Cyan: ", cyan)
    print("Magenta: ", magenta)
    print("Yellow: ", yellow)


elif choice =='2':
    print()
    cyan = int(input("value of cyan(0-100): "))
    magenta = int(input("value of magenta(0-100): "))
    yellow = int(input("value of yellow(0-100): "))

    red = round(255*(1-(cyan/100)))
    green = round(255*(1-(magenta/100)))
    blue = round(255*(1-(yellow/100)))
    print()
    print("Red: ", red)
    print("Green: ", green)
    print("Blue: ", blue)


elif choice == '3':
    print()
    red = int(input("value of red(0-255): "))/255
    green = int(input("value of green(0-255): "))/255
    blue = int(input("value of blue(0-255): "))/255

    dicti = {'red': red, 'green': green, 'blue': blue}
    max_key = max(dicti, key=dicti.get)

    list = [red, green, blue]
    max = max(list)
    min = min(list)

    lum = ((min + max)/2)*100

    if (lum/100) >= 0.5:
        sat = ((max-min)/(2-max-min))*100
    else:
        sat = ((max-min)/(max+min))*100

    if max_key == 'red':
        hue = 60*((green-blue)/(max-min))
    elif max_key == 'green':
        hue = 60*(2+(blue-red)/(max-min))
    elif max_key == 'blue':
        hue = 60*(4+(red-green)/(max-min))

    if hue < 0:
        hue += 360

    print()
    print('Saturation: ', round(sat))
    print('Lumination: ', round(lum))
    print('Hue: ', round(hue))


elif choice =='4':
    print()
    sat = int(input("enter value of saturation(0-100): "))/100
    lum = int(input("enter value of lumination(0-100): "))/100
    hue = int(input("enter value of hue(0-360): "))/360

    if 100<sat<0:
        
        if sat == 0:
            red = 255*lum
            green = 255*lum
            blue = 255*lum

    if lum >= 0.5:
        temp1 = (lum+sat)-(lum*sat)
    else:
        temp1 = lum*(1+sat)

    temp2 = 2*lum-temp1

    tempR = hue+0.333
    tempG = hue
    tempB = hue-0.333

    if tempR < 0:
        tempR += 1
    elif tempG < 0:
        tempG += 1
    elif tempB < 0:
        tempB += 1
    elif tempR > 1:
        tempR -= 1
    elif tempG > 1:
        tempG -= 1
    elif tempB > 1:
        tempB -= 1

    # vind rood
    if (6*tempR) < 1:
        red = 255*(temp2 + (temp1-temp2)*6*tempR)
    elif (2*tempR) < 1:
        red = 255*temp1
    elif (3*tempR) < 2:
        red = 255*(temp2 + (temp1-temp2) * (0.666-tempR) * 6)
    else:
        red = 255*temp2

    # vind groen
    if (6*tempG) < 1:
        green = 255*(temp2 + (temp1-temp2)*6*tempG)
    elif (2*tempG) < 1:
        green = 255*temp1
    elif (3*tempG) < 2:
        green = 255*(temp2 + (temp1-temp2) * (0.666-tempG) * 6)
    else:
        green = 255*temp2

    # vind blauw
    if (6*tempB) < 1:
        blue = 255*(temp2 + (temp1-temp2)*6*tempB)
    elif (2*tempB) < 1:
        blue = 255*temp1
    elif (3*tempB) < 2:
        blue = 255*(temp2 + (temp1-temp2) * (0.666-tempB) * 6)
    else:
        blue = 255*temp2

    print()
    print("Red: ", round(red))
    print("Green: ", round(green))
    print("Blue: ", round(blue))
