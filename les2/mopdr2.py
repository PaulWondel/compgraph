#!/bin/python
# script to convert color notations
# RGBCMYSL values range from 0 to 1, H ranges from 0 to 360


##############################################################################
# RGB CMY
##############################################################################

def RGBtoCMY(R, G, B):
        # Calculate CMY from RGB input
        C = 1-R
        M = 1-G
        Y = 1-B
	
        return (C,M,Y)

def CMYtoRGB(C, M, Y):
        # Calculate RGB from CMY input
        R = 1-C
        G = 1-M
        B = 1-Y
        
        return (R, G, B)

##############################################################################
# RGB HSL
##############################################################################

def RGBtoHSL(R, G, B):
        # Get the biggest and smallest value from RGB
        Max = max((R,G,B))
        Min = min((R,G,B))

        # difference between max and min
        D = Max - Min

        # Calculating Hue acording to the biggest RGB value
        # if the difference between Max and Min is 0, then so is H
        if(D == 0):
                H = 0
        elif(Max == R):
                H = 60 * (((G-B)/D)%6)
        elif(Max == G):
                H = 60 * (((B-R)/D)+2)
        elif(Max == B):
                H = 60 * (((R-G)/D)+4)

        # Lightness is the avarage between max and min
        L = (Max + Min) / 2
        
        # Calculating Saturation acording to the Lightness value
        # if the difference between Max and Min is 0, then so is D
        if(D == 0):
            S = 0
        elif(L <= 0.5):
            S = (Max-Min) / (Max+Min)
        elif(L >= 0.5):
            S = (Max-Min) / (2.0 - Max - Min)

        return((H,S,L))

def HSLtoRGB(H,S,L):
        C = (1 - abs(2*L -1)) * S
        X = C * (1 - abs((H/60)%2-1))
        M = L - C/2

        if(H < 60):
                rgb = (C,X,0)
        elif(H < 120):
                rgb = (X,C,0)
        elif(H < 180):
                rgb = (0,C,X)
        elif(H < 240):
                rgb = (0,X,C)
        elif(H < 300):
                rgb = (X,0,C)
        elif(H < 360):
                rgb = (C,0,X)

        # Calculate RGB values
        R = (rgb[0]+M) * 255
        G = (rgb[1]+M) * 255
        B = (rgb[2]+M) * 255
        
        return((R,G,B))

##############################################################################

def transparency(R1,G1,B1,alpha1,R2,G2,B2):
    # Calculate new value for color 1 as layer over color 2
    R3 = alpha1 * R1 + (1-alpha1) * R2
    G3 = alpha1 * G1 + (1-alpha1) * G2
    B3 = alpha1 * B1 + (1-alpha1) * B2

    return((R3,G3,B3))

##############################################################################

print(RGBtoCMY(0.4, 0.5, 0.6)) # (0.6, 0.5, 0.4)
print(CMYtoRGB(0.4, 0.5, 0.6)) # (0.6, 0.5, 0.4)
print(RGBtoHSL(0.4, 0.5, 0.6)) # (210.0, 0.2, 0.5)
print(HSLtoRGB(100, 0.5, 0.6)) # (0.533, 0.8, 0.4)
print(transparency(0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0)) # (0.52, 0.62, 0.72)
