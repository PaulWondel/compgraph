cmy_scale = 100
rgb_scale = 255
key=0

# RGB to CMY 
def rgbtocmy(r,g,b):
	if (r==0) and (g==0) and (b==0):
		#black
		return 0, 0, 0, cmy_scale
	# rgb [0,255] -> cmy [0,1]
	c = 1-r/255
	m = 1-g/255
	y = 1-b/255

	#extract out k [0,1]
	min_cmy = min(c,m,y)
	c = (c - min_cmy)/(1-min_cmy)
	m = (m - min_cmy)/(1-min_cmy)
	y = (y - min_cmy)/(1-min_cmy)
	key = min_cmy

	# rescale to range [0,cmyk_scale]
	return c*cmy_scale, m*cmy_scale, y*cmy_scale, key*cmy_scale

# CMY to RGB
def cmytorgb(c,m,y):
	
	r = rgb_scale*(1.0-(c+key)/float(cmy_scale))
	g = rgb_scale*(1.0-(m+key)/float(cmy_scale))
	b = rgb_scale*(1.0-(y+key)/float(cmy_scale))
	
	return r, g, b


################################################################################
# RGB to HSL
def rgbtohsl(R,G,B):
	#Get Biggest and smallest value from RGB
	Max = max((R,G,B))
	Min = min((R,G,B))

	#Difference between max and min
	D = Max - Min

	#Calculaing Hue according to the biggest RGB value
	#if the between Max and Min is 0, then so is H
	if(D==0):
		H=0
	elif(Max==R):
		H=60*(((G-B)/D)%6)
	elif(Max==G):
		H=60*(((B-R)/D)+2)
	elif(Max==B):
		H=60*(((R-G)/D)+4)
	
	# Lightness is the average between max and min
	L=(Max + Min)/2
	
	# Calculating Saturation according to the Lightness Value
	# if the difference between Max and Min is 0, then so is D
	if(D==0):
		S=0
	elif(L<=0.5):
		S=(Max-Min)/(Max+Min)
	elif(L>=0.5):
		S=(Max-Min)/(2.0-Max-Min)
	
	return((H,S,L))

#HSL to RGB
def hsltorgb(H,S,L):
	C=(1-abs(2*L-1))*S
	X=C*(1-abs((H/60)%2-1))
	M=L-C/2

	if(H<60):
		rgb=(C,X,0)
	elif(H<120):
		rgb=(X,C,0)
	elif(H<180):
		rgb=(0,C,X)
	elif(H<240):
		rgb=(0,X,C)
	elif(H<300):
		rgb=(X,0,C)
	elif(H<360):
		rgb=(C,0,X)

	#Calculate RGB values
	R=(rgb[0]+M)*255
	G=(rgb[1]+M)*255
	B=(rgb[2]+M)*255
	
	return((R,G,B))


################################################################################
# Transparency
def transparency(R1,G1,B1,alpha1,R2,G2,B2):
	R3=alpha1*R1+(1-alpha1)*R2
	G3=alpha1*G1+(1-alpha1)*G2
	B3=alpha1*B1+(1-alpha1)*B2

	return((R3,G3,B3))

################################################################################

print(rgbtocmy(0.4, 0.5, 0.6)) #(0.6, 0.5, 0.4)
print(cmytorgb(0.4, 0.5, 0.6)) #(0.6, 0.5, 0.4)
print(rgbtohsl(0.4, 0.5, 0.6)) #(210.0, 0.2, 0.5)
print(hsltorgb(100, 0.5, 0.6)) #(0.533, 0.8, 0.4)
print(transparency(0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0)) #(0.52, 0.62, 0.72)





