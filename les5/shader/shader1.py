# PyOpenGL imports
from OpenGL.GLUT import *
from OpenGL.GL import *

# Import the vertex buffer object
from OpenGL.arrays import vbo

from numpy import array

def display():
    # Clear the buffer
    glClear(GL_COLOR_BUFFER_BIT)
    # Tell opengl to draw the vertices
    # in the VBO as a series of triangles
    glDrawArrays(GL_TRIANGLES, 0, 9)
    glFlush()

# Create window
glutInit()
glutCreateWindow("Shader Demo".encode("ascii"))

# Compile vertex shader
vsSource = '''
#version 120
void main() {
    gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;
}
'''
vs = glCreateShader(GL_VERTEX_SHADER)
glShaderSource(vs, vsSource)
glCompileShader(vs)
    
# Compile fragment shader
fsSource = '''
#version 120
void main() {
    gl_FragColor = vec4(0,1,0,1);
}
'''
fs = glCreateShader(GL_FRAGMENT_SHADER)
glShaderSource(fs, fsSource)
glCompileShader(fs)

# Combine the two shaders into a program
program = glCreateProgram()
glAttachShader(program, vs)
glAttachShader(program, fs)
glLinkProgram(program)
glUseProgram(program)

# Create a Vertex Buffer Object and fill it with data
vbo = vbo.VBO(array([
    [  0,    1, 0 ],
    [ -1,   -1, 0 ],
    [  1,   -1, 0 ],
    [ -1,    0, 0 ],
    [ -0.5,  0, 0 ],
    [ -0.75, 1, 0 ],
    [  0.5,  0, 0 ],
    [  1,    0, 0 ],
    [  0.75, 1, 0 ]
], 'f'))
# Bind the VBO
vbo.bind()
# Tell opengl to access the vertex array once
# we call a draw function
glEnableClientState(GL_VERTEX_ARRAY)
# Point to our vbo data
glVertexPointerf(vbo)

glutDisplayFunc(display)
glutMainLoop()
