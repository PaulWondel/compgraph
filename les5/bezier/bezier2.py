# Bezier patch

from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *

from sys import exit

SPEED = 0.02

def display():
    nPoints = 20 # aantal punten per curve
    nCurves = 8 # aantal curves in beide richtingen
    phi = SPEED * glutGet(GLUT_ELAPSED_TIME)
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
    glPushMatrix()
    glRotate(phi, 0, 1, 0)
    glPointSize(5)
    glColor(1, 1, 0) # geel
    glBegin(GL_POINTS) # teken de control points
    for points in controlPoints:
        for point in points:
            glVertex3fv(point) 
    glEnd()
    glColor(1, 1, 1) # wit
    for j in range(nCurves + 1):
        glBegin(GL_LINE_STRIP) # teken de curves in de ene richting
        for i in range(nPoints + 1):
            glEvalCoord2f(float(i) / nPoints, float(j) / nCurves)
        glEnd()
        glBegin(GL_LINE_STRIP) # teken de curves in de andere richting
        for i in range(nPoints + 1):
            glEvalCoord2f(float(j) / nCurves, float(i) / nPoints)
        glEnd()
    glPopMatrix()
    glutSwapBuffers()

def end(key, x, y):
    exit()

glutInit()
glutInitDisplayMode(GLUT_MULTISAMPLE | GLUT_DOUBLE | GLUT_DEPTH)
glutInitWindowSize(640, 480)
glutCreateWindow("Perspective view".encode("ascii"))
glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)
glEnable(GL_BLEND)
glEnable(GL_LINE_SMOOTH)
glEnable(GL_DEPTH_TEST)
glMatrixMode(GL_PROJECTION)
glFrustum(-1.333, 1.333, -1, 1, 5, 20)
glMatrixMode(GL_MODELVIEW)
gluLookAt(6, 8, 10, 0, 0, 0, 0, 1, 0)
controlPoints = ((-1.5, -1.5, -4), (-0.5, -1.5, -2), \
                 (0.5, -1.5, 1), (1.5, -1.5, -2)), \
                ((-1.5, -0.5, -1), (-0.5, -0.5, -3), \
                 (0.5, -0.5, 0), (1.5, -0.5, 1)), \
                ((-1.5, 0.5, -4), (-0.5, 0.5, 0), \
                 (0.5, 0.5, -3), (1.5, 0.5, -4)), \
                ((-1.5, 1.5, 2), (-0.5, 1.5, 2), \
                 (0.5, 1.5, 0), (1.5, 1.5, 1))
glMap2f(GL_MAP2_VERTEX_3, 0, 1, 0, 1, controlPoints) # 2D "evaluator"
glEnable(GL_MAP2_VERTEX_3)
glutDisplayFunc(display)
glutKeyboardFunc(end)
glutIdleFunc(glutPostRedisplay)
glutMainLoop()
