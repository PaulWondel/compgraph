# Bezier curve

from OpenGL.GLUT import *
from OpenGL.GLU import *
from OpenGL.GL import *

from sys import exit

def display():
    nPoints = 20 # aantal lijnsegmenten in de curve
    glPointSize(5)
    glColor(1, 1, 0) # geel
    glBegin(GL_POINTS) # teken de control points
    for point in controlPoints:
        glVertex3fv(point)
    glEnd()
    glColor(1, 1, 1) # wit
    glBegin(GL_LINE_STRIP) # teken de curve
    for i in range(nPoints + 1):
        glEvalCoord1f(float(i) / nPoints)
    glEnd()
    glColor(0, 1, 0) # groen
    glBegin(GL_POINTS) # teken de punten van de curve
    for i in range(1, nPoints):
        glEvalCoord1f(float(i) / nPoints)
    glEnd()
    glFlush()

def end(key, x, y):
    exit()
	
glutInit()
glutInitDisplayMode(GLUT_MULTISAMPLE)
glutInitWindowSize(640, 480)
glutCreateWindow("Orthographic view".encode("ascii"))
glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)
glEnable(GL_BLEND)
glEnable(GL_LINE_SMOOTH)
controlPoints = (-0.8, -0.8, 0), (-0.4, 0.8, 0), (0.4, -0.8, 0), (0.7, 0.8, 0)
glMap1f(GL_MAP1_VERTEX_3, 0, 1, controlPoints) # definieer een "evaluator"
glEnable(GL_MAP1_VERTEX_3) # zet 'm aan
glutDisplayFunc(display)
glutKeyboardFunc(end)
glutMainLoop()
