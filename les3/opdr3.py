from grid import *
import math

# Vergelijking van een lijn y=ax+b
def rasterline(x1,y1,x2,y2):
	# Rasterveld opstellen 
	g = Grid(max(x1,x2)+5,max(y1,y2)+5)	

	# Zorgen dat a van y=ax+b niet oneindig wordt als de lijn verticaal is. 
	if x1==x2:
		x2=x2+.1	

	# Berekening van dy/dx
	a = (y2-y1)/(x2-x1)
	print('a',a)

	# if a <1, loopt door alle x coordinaten en vult de hokjes
	# else vul y coordinaten
	# Zorgt voor geen open hokjes in een lijn

	# als the lijn van rechts naar links gaat, dan stap=-1
	stap=1

	if abs(a)<1:
		if x1>x2:
			stap=-1
		for x in range(x1,x2,stap):
			# Bresenham-algoritme
			y=round(a*(x-x1))+y1
			g.addPoint(x,y,(1,1,1))
	else:
		if y1>y2:
			stap=-1
		for y in range(y1,y2,stap):
			# Bresenham-algoritme
			x=round(1/a * (y-y1))+x1
			g.addPoint(x,y,(1,1,1))

	# Tekent input coordinaten
	g.addPoint(x1,y1,(1,0,0))
	g.addPoint(x2,y2,(0,.5,1))
	g.draw()
	
def main():
	# rood naar blauw
	rasterline(1,47,30,5)

if __name__== "__main__":
	main()
