import pygame
from pygame.locals import *
from OpenGL.GL import *
from OpenGL.GLU import *
import numpy

class Road:
	ground_vertices = (
    (-10,-0.1,20),
    (20,-0.1,20),		
    (20,-0.1,-20),
    (-10,-0.1,-20)
)

	def __init__(self):
		self.ground_vertices = Road.ground_vertices

	def ground(self):
			glBegin(GL_QUADS)
			for vertex in self.ground_vertices:
					glColor3fv((1,0.5,0.5))
					glVertex3fv(vertex)
			glEnd()

class Pyramid:
	vertices = [
		[1,-1,-1],			#back right
		[1,-1,1],			#front right
		[-1,-1,1],			#front left
		[-1,-1,-1],			#back left
		[0,1,0]				#top
	]
	
	edges = (
		(0,1),
		(0,3),
		(0,4),
		(1,4),
		(1,2),
		(2,4),
		(2,3),
		(3,4)
	)
	
	surfaces = (
		(1,2,4,1),
		(0,1,2,3),			#bottom surface
		(0,1,4,0),	
		(0,3,4,0),
		(2,3,4,2),
	)

	colors = (
    (1,0,0),
    (0,1,0),
    (0,0,1),
    (0,1,0),
    (1,1,1),
    (0,1,1),
    (1,0,0),
    (0,1,0),
    (0,0,1),
    (0,1,0),
    (1,1,1),
    (0,1,1)
)
		
	def __init__(self, mul=1): # mul to multiply the vertices of the pyramid to increase the size of the pyramid
		self.edges = Pyramid.edges
		self.vertices = list(numpy.multiply(numpy.array(Pyramid.vertices), mul))
		self.surfaces = Pyramid.surfaces
		self.colors = Pyramid.colors

	def draw(self):
		self.draw_sides()
		glLineWidth(3)
		glBegin(GL_LINES)
		for edge in self.edges:
			for vertex in edge:
				glVertex3fv(self.vertices[vertex])
				glColor3f(0,0,1)
		glEnd()

	def draw_sides(self):
		glBegin(GL_QUADS)
		for surface in self.surfaces:
			x=0
			for vertex in surface:
				x+=1
				glColor3fv(self.colors[x])
				glVertex3fv(self.vertices[vertex])
				# glColor3f(0,1,0)
		glEnd()

	def move(self, x, y, z): # changes the value of the vertices
		self.vertices = list(map(lambda vert:(vert[0]+x, vert[1]+y,vert[2]+z), self.vertices))

class Cube:
	vertices = (
    (1,-1,-1), #0
    (1,1,-1), #1
    (-1,1,-1), #2
    (-1,-1,-1), #3
    (1,-1,1), #4
    (1,1,1), #5
    (-1,1,1), #6
    (-1,-1,1) #7 
	)

	# edges to show which vertex connects to which other vertex
	edges = (
    (0,1),
    (0,3),
    (0,4),
    (2,1),
    (2,3),
    (2,6),
    (5,1),
    (5,4),
    (5,6),
    (7,3),
    (7,4),
    (7,6)
)
	
	surfaces = (
    (0,1,2,3),
    (2,3,7,6),
    (4,5,6,7),
    (0,1,5,4),
    (1,2,6,5),
    (0,3,7,4)
)	

	colors = (
    (1,0,0),
    (0,1,0),
    (0,0,1),
    (0,1,0),
    (1,1,1),
    (0,1,1),
    (1,0,0),
    (0,1,0),
    (0,0,1),
    (0,1,0),
    (1,1,1),
    (0,1,1)
)

	def __init__(self, mul=1): # mul to multiply the vertices of the pyramid to increase the size of the pyramid
		self.edges = Cube.edges
		self.vertices = list(numpy.multiply(numpy.array(Cube.vertices), mul))
		self.surfaces = Cube.surfaces
		self.colors = Cube.colors

	def draw(self):
		self.draw_sides()
		glLineWidth(3)
		glBegin(GL_LINES)
		for edge in self.edges:
			for vertex in edge:
				glVertex3fv(self.vertices[vertex])
				glColor3f(0,0,1)
		glEnd()

	def draw_sides(self):
		glBegin(GL_QUADS)
		for surface in self.surfaces:
			x=0
			for vertex in surface:
				x+=1
				glColor3fv(self.colors[x])
				glVertex3fv(self.vertices[vertex])
				# glColor3f(0,1,0)
		glEnd()

	def move(self, x, y, z): # changes the value of the vertices
		self.vertices = list(map(lambda vert:(vert[0]+x, vert[1]+y,vert[2]+z), self.vertices))


def main():
	pygame.init()
	display = (1600,900)
	pygame.display.set_mode(display, DOUBLEBUF|OPENGL)

	global play_pause
	play_pause = False #for auto rotation

 # Camera settings
	gluPerspective(45,(display[0]/display[1]),0.1,50)
	# glRotatef(45,1,5,0)
	# glTranslatef(10, -5,-10)	# Moves the camera with the given matrix
	gluLookAt(20, 5, 20, 0, 0, 0, 0, 1, 0)

	p = Pyramid(1)
	p.move(0,1,0)
	p1 = Pyramid(1)
	p1.move(0,1,15)
	c1 = Cube(1)
	c1.move(0,3.1,0) #position
	c2 = Cube(1)
	c2.move(0,1,-10)

	r = Road()	
	
	glLight(GL_LIGHT0, GL_POSITION, [5,5,5,1])
	glLight(GL_LIGHT0, GL_DIFFUSE, [1,1,1,1])
	glLight(GL_LIGHT0, GL_AMBIENT, [0,0,0,1])
	glLight(GL_LIGHT0, GL_SPECULAR, [1,1,1,1])
	glEnable(GL_DEPTH_TEST)
	
	vel = 0.2 # Variable outside of while loop for camera
	turn = 1

	clock = pygame.time.Clock()
	while True:
		clock.tick(60)
		for event in pygame.event.get():
			if event.type == pygame.QUIT:
				pygame.quit()
				quit()
			if event.type == pygame.KEYDOWN:
				if event.key == pygame.K_p:
					if play_pause == False:
						play_pause = True
					else:
						play_pause = False

		# Camera functions
		if event.type == pygame.KEYDOWN:
			if event.key == pygame.K_LEFT:
					glTranslatef(-vel,0,0)
			if event.key == pygame.K_RIGHT:
					glTranslatef(vel,0,0)
			if event.key == pygame.K_DOWN:
					glTranslatef(0,vel,0)
			if event.key == pygame.K_UP:
					glTranslatef(0,-vel,0)
			if event.key == pygame.K_KP8:
					glTranslatef(0,0,vel)
			if event.key == pygame.K_KP2:
					glTranslatef(0,0,-vel)
			if event.key == pygame.K_KP6:
					glRotate(1,0,turn,0)
			if event.key == pygame.K_KP4:
					glRotate(1,0,-turn,0)
		
		
		keys = pygame.key.get_pressed()
		if play_pause == True:
			if keys[pygame.K_a]: # move objects left
				p.move(-vel,0,0)
				c1.move(-vel,0,0)
			if keys[pygame.K_d]: # move objects right
				p.move(vel,0,0)
				c1.move(vel,0,0)
			if keys[pygame.K_r]: # move objects up
				p.move(0,vel,0)
				c1.move(0,vel,0)
			if keys[pygame.K_f]: #move objects down
				p.move(0,-vel,0)
				c1.move(0,-vel,0)
			if keys[pygame.K_w]:# move objects forward
				p.move(0,0,-vel)
				c1.move(0,0,-vel)
			if keys[pygame.K_s]:# move objects Backward
				p.move(0,0,vel)
				c1.move(0,0,vel)
			if keys[pygame.K_e]:
				glRotatef(turn,0,1,0)	
			if keys[pygame.K_q]:
				glRotatef(-turn,0,1,0)
		elif play_pause == False:
			glRotatef(1, 0, 1, 0) # Continuous rotation
		if keys[pygame.K_o]:
			pygame.quit()
			quit()

		# glRotatef(2,1,1,3)	# Rotates the camera around the given matrix
		glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT)
		glEnable(GL_LIGHTING)
		glEnable(GL_COLOR_MATERIAL)
		glEnable(GL_LIGHT0)		
		glColorMaterial(GL_FRONT_AND_BACK,GL_EMISSION) 

		
		r.ground()
		p.draw()
		p1.draw()
		c1.draw()
		c2.draw()
		pygame.display.flip()

		glDisable(GL_LIGHT0)
		glDisable(GL_LIGHTING)
		glDisable(GL_COLOR_MATERIAL)

main()
