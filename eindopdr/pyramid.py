#! /usr/bin/env python3

import pygame
from pygame.locals import *
from OpenGL.GL import *
from OpenGL.GLU import *
import numpy
import glfw
from PIL import Image


class Pyramid:
    vertices = [
        [1, -1, -1],
        [1, -1, 1],
        [-1, -1, 1],
        [-1, -1, -1],
        [0, 1, 0]
    ]

    edges = (
        (0, 1),
        (0, 3),
        (0, 4),
        (1, 4),
        (1, 2),
        (2, 4),
        (2, 3),  # (2,3)
        (3, 4)
    )

    surfaces = (
        (1,2,4),
        (0,1,2,3),
        (0,1,4),
        (0,3,4),
        (2,3,4)
    )

    def __init__(self, mul=1):
        self.edges = Pyramid.edges
        self.vertices = list(numpy.multiply(numpy.array(Pyramid.vertices), mul))
        self.surfaces = Pyramid.surfaces

    def draw(self):
        self.draw_sides()
        glLineWidth(10)
        glBegin(GL_LINES)
        for edge in self.edges:
            for vertex in edge:
                glColor3f(0, 1, 0)
                glVertex3fv(self.vertices[vertex])

        glEnd()

    def draw_sides(self):
        glBegin(GL_QUADS)
        for surface in self.surfaces:
            for vertex in surface:
                glColor3f(1, 0, 0)
                glVertex3fv(self.vertices[vertex])

        glEnd()

    def move(self, x, y, z):
        self.vertices = list(map(lambda vert: (vert[0] + x, vert[1] + y, vert[2] + z), self.vertices))

class Cube():
    vertices = (
        (1+3, -1+3, -1+3),
        (1+3, 1+3, -1+3),
        (-1+3, 1+3, -1+3),
        (-1+3, -1+3, -1+3),
        (1+3, -1+3, 1+3),
        (1+3, 1+3, 1+3),
        (-1+3, -1+3, 1+3),
        (-1+3, 1+3, 1+3)
        )

    edges = (
        (0,1),
        (0,3),
        (0,4),
        (2,1),
        (2,3),
        (2,7),
        (6,3),
        (6,4),
        (6,7),
        (5,1),
        (5,4),
        (5,7)
        )
 
    surfaces = (
        (0,1,2,3),
        (0,1,4,5),
        (1,3,5,7),
        (0,2,4,6),
        (2,3,6,7),
        (4,5,6,7)
        )

    def __init__(self, mul=1):
        self.edges = Cube.edges
        self.vertices = list(numpy.multiply(numpy.array(Cube.vertices), mul))
        self.surfaces = Cube.surfaces

    def draw(self):
        self.draw_sides()
        glLineWidth(10)
        glBegin(GL_LINES)
        for edge in self.edges:
            for vertex in edge:
                glColor3f(0, 1, 0)
                glVertex3fv(self.vertices[vertex])

        glEnd()

    def draw_sides(self):
        glBegin(GL_QUADS)
        for surface in self.surfaces:
            for vertex in surface:
                glColor3f(1, 0, 0)
                glVertex3fv(self.vertices[vertex])

        glEnd()

    def set_vertices(max_distance):
        x_value_change = random.randrange(-10,10)
        y_value_change = random.randrange(-10,10)
        z_value_change = random.randrange(-1*max_distance,-20)
        new_vertices = []
    
        for vert in vertices:
            new_vert = []   
            new_x = vert[0] + x_value_change
            new_y = vert[1] + y_value_change
            new_z = vert[2] + z_value_change
            new_vert.append(new_x)
            new_vert.append(new_y)
            new_vert.append(new_z)
    
            new_vertices.append(new_vert)
    
        return new_vertices



    def move(self, x, y, z):
        self.vertices = list(map(lambda vert: (vert[0] + x, vert[1] + y, vert[2] + z), self.vertices))

def main():
    if not glfw.init():
        return

    pygame.init()
    # display = glfw.create_window(800, 800, "Kaas", None, None)
    display = (800,800)

    if not display:
        glfw.terminate()
        return

    pygame.display.set_mode(display, DOUBLEBUF|OPENGL)

    gluPerspective(45, (display[0]/display[1]), 0.1, 50)

    glTranslatef(0,0,-20)
    glEnable(GL_DEPTH_TEST)

    p = Pyramid(2)
    c = Cube(2)
    glTranslatef(0,0,-20)

    vel = 0.1
    jump = -1
    clock = pygame.time.Clock()
    while True:
        clock.tick(60)
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()

        keys = pygame.key.get_pressed()
        if keys[pygame.K_d]:
            p.move(-vel, 0, 0)
            c.move(-vel, 0, 0)
        if keys[pygame.K_a]:
            p.move(vel, 0, 0)
            c.move(vel, 0, 0)
        if keys[pygame.K_g]:
            p.move(0, vel, 0)
            c.move(0, vel, 0)
        if keys[pygame.K_t]:
            p.move(0, -vel, 0)
            c.move(0, -vel, 0)
        if keys[pygame.K_w]:
            p.move(0, 0, vel)
            c.move(0, 0, vel)
        if keys[pygame.K_s]:
            p.move(0, 0, -vel)
            c.move(0, 0, -vel)
        

        glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT)
        p.draw()
        c.draw()
        pygame.display.flip()


main()
