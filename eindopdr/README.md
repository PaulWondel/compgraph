# Gebruikersaanwijzingen

De volgende knoppen zijn om de camera te besturen:
- P -> pauseert constante rotatie en gaat over naar handmatige besturing
- UP -> naar boven (y-as)
- DOWN -> naar beneden (y-as)
- LEFT, RIGHT -> Naar links en rechts (x-as)
- Q, E -> Roteert om het object

De volgende knoppen zijn om het gecentreerd object te bewegen:
- W, S -> naar voren en achter (x-as)
- A, D -> links en rechts (z-as)
- R, F -> naar boven en beneden (y-as)

Move the camera:
- P -> pause animation and switch over to manual
- UP -> go up (y-as)
- DOWN -> go down (y-as)
- LEFT, RIGHT -> go left or right(x-as)
- Q, E -> rotate around the object

Move the centered object:
- W, S -> To front, back (x-axis)
- A, D -> left, right (z-axis)
- R, F -> up, down (y-axis)