# Use named attributes in vertex shader

# PyOpenGL imports
from OpenGL.GLUT import *
from OpenGL.GL import *

# Import shaders
from OpenGL.GL import shaders

import numpy as np

# Create window
glutInit()
glutCreateWindow("Shader Demo".encode("ascii"))

# Compile vertex shader
vsSource = '''#version 120
attribute vec3 pos;
void main() {
	gl_Position = vec4(pos, 1.0);
}
'''
vs = shaders.compileShader(vsSource, GL_VERTEX_SHADER)

# Compile fragment shader
fsSource = '''#version 120
void main() {
	gl_FragColor = vec4(0.5, 0.5, 1.0, 1.0);
}
'''
fs = shaders.compileShader(fsSource, GL_FRAGMENT_SHADER)

# Combine the two shaders into a program
program = shaders.compileProgram(vs, fs)
glUseProgram(program)

# Create an arrayBuffer and fill it with data
buffer = glGenBuffers(1)
glBindBuffer(GL_ARRAY_BUFFER, buffer)
glBufferData(GL_ARRAY_BUFFER, np.array([
    -1,  0, 0,
     0,  1, 0,
     0, -1, 0,
     1,  0, 0
], 'f'), GL_STATIC_DRAW)

# Point the attribute pos to the arrayBuffer
attr = glGetAttribLocation(program, "pos")
glEnableVertexAttribArray(attr)
glVertexAttribPointer(attr, 3, GL_FLOAT, GL_FALSE, 0, None) # 3 means: take 3 items from the buffer per vertex

# Set the background color
glClearColor(0.8, 0.8, 0.8, 1) # r, g, b, alpha

def display():
    glClear(GL_COLOR_BUFFER_BIT)
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4)
    glFlush()

glutDisplayFunc(display)
glutMainLoop()
