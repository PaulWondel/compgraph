#!/bin/python
from grid import *
import math 

def rasterline(x1, y1, x2, y2):
    # create grid "g" Class with the maximum values
    g = Grid(max(x1,x2)+5, max(y1,y2)+5)

    # make shure variable "a" wil not be infinity if the line is vertical.
    if x1 == x2:
        x2 = x2 + .1

    # calculate dy/dx
    a = (y2 - y1) / (x2 - x1); print('a', a)

    # if a < 1 then loop through all x coordinates, and fill them up.
    # else fill up all y coordinates.
    # this makes sure that there will not be gaps in between a line

    # if the line go's from right to left, the step should be -1
    step = 1

    if abs(a) < 1:
        if x1 > x2:
            step = -1
        for x in range(x1, x2, step):
            # Bresenham-algoritme
            y = round(a * (x - x1)) + y1
            g.addPoint(x, y, (1, 1, 1))
    else:
        if y1 > y2:
            step = -1
        for y in range(y1, y2,step):
            # Bresenham-algoritme
            x = round(1/a * (y - y1)) + x1
            g.addPoint(x, y, (1, 1, 1))

    # draw input coordinates
    g.addPoint(x1, y1, (1, 0, 0))
    g.addPoint(x2, y2, (0, .5, 1))
    g.draw()

def main():
    # red to blue
    rasterline(57,7,27,19)

if __name__== "__main__":
    main()
