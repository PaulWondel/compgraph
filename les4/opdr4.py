from lines import *
import math

l = Lines(1200, 800)
#l.addLine((100, 100),(500,300))
#l.draw()

# factor voor vector om lijnen te kunnen tekenen
length_of_lines = 100

# Homogene coordinaten kubus
v1=(-1,-1,-1)
v2=(1,-1,-1)
v3=(1,1,-1)
v4=(-1,1,-1)
v5=(-1,-1,1)
v6=(1,-1,1)
v7=(1,1,1)
v8=(-1,1,1)

def Orthographic(matrix, origin, p1, p2):

    l.addLine(
        (
            (matrix[0] * p1[0] + matrix[1] * p1[1]) * length_of_lines + origin[0],  # x1 = 'matrix row 0' * p1_xy
            (matrix[2] * p1[0] + matrix[3] * p1[1]) * length_of_lines + origin[1]  # y1 = 'matrix row 1' * p1_xy
        ),
        (

            (matrix[0] * p2[0] + matrix[1] * p2[1]) * length_of_lines + origin[0],  # x2 = 'matrix row 0' * p2_xy
            (matrix[2] * p2[0] + matrix[3] * p2[1]) * length_of_lines + origin[1]  # y2 = 'matrix row 1' * p2_xy
        )
    )


def Convert(matrix, origin, p1, p2):
    # for example:
    # (1, 0, 0.5) (p1[0])
    #             (p1[1])
    #             (p1[2])

    # (0, 1, 0.5) (p2[0])
    #             (p2[1])
    #             (p2[2])

    l.addLine(
        (
            (matrix[0] * p1[0] + matrix[1] * p1[1] + matrix[2] * p1[2]) * length_of_lines + origin[0],
            # x1 = 'matrix row 0' * p1_xyx
            (matrix[3] * p1[0] + matrix[4] * p1[1] + matrix[5] * p1[2]) * length_of_lines + origin[1]
            # y1 = 'matrix row 1' * p1_xyx
        ),
        (

            (matrix[0] * p2[0] + matrix[1] * p2[1] + matrix[2] * p2[2]) * length_of_lines + origin[0],
            # x2 = 'matrix row 0' * p2_xyx
            (matrix[3] * p2[0] + matrix[4] * p2[1] + matrix[5] * p2[2]) * length_of_lines + origin[1]
            # y2 = 'matrix row 1' * py_xyx
        )
    )


def rotate(degrees, p1):
    #    (cosP, 0, sinP     ) (p1[0]) X
    #    (0, 1, 0           ) (p1[1]) Y
    #    (-sinP, 0, cosP    ) (p1[2]) Z

    # van graden naar radialen. radialen worden gebruikt bij cos en sin
    rad = degrees * math.pi / 180

    # y-as rotation
    matrix = (math.cos(rad), 0, math.sin(rad),
              0, 1, 0,
              math.sin(rad) * -1, 0, math.cos(rad))

    return ((matrix[0] * p1[0] + matrix[1] * p1[1] + matrix[2] * p1[2]),
            (matrix[3] * p1[0] + matrix[4] * p1[1] + matrix[5] * p1[2]),
            (matrix[6] * p1[0] + matrix[7] * p1[1] + matrix[8] * p1[2]))


#Kubus functies oproepen

originOrthographic = (200, 200)  # center van de kubus
matrix1 = (1, 0,
           0, 1)

Orthographic(matrix1, originOrthographic, v1, v2)  # A
Orthographic(matrix1, originOrthographic, v2, v3)  # B
Orthographic(matrix1, originOrthographic, v3, v4)  # C
Orthographic(matrix1, originOrthographic, v4, v1)  # D

Orthographic(matrix1, originOrthographic, v1, v5)  # Wordt niet getekend als X en Y hetzelfde zijn
Orthographic(matrix1, originOrthographic, v2, v6)  # Van (0,0) naar (0,0)
Orthographic(matrix1, originOrthographic, v3, v7)  #
Orthographic(matrix1, originOrthographic, v4, v8)  #

Orthographic(matrix1, originOrthographic, v5, v6)  # A
Orthographic(matrix1, originOrthographic, v6, v7)  # B
Orthographic(matrix1, originOrthographic, v7, v8)  # C
Orthographic(matrix1, originOrthographic, v8, v5)  # D

originParallel = (600, 200)  # center van kubus
matrix2 = (1, 0, 0.5,
           0, 1, 0.5)

Convert(matrix2, originParallel, v1, v2)
Convert(matrix2, originParallel, v2, v3)
Convert(matrix2, originParallel, v3, v4)
Convert(matrix2, originParallel, v4, v1)
Convert(matrix2, originParallel, v1, v5)
Convert(matrix2, originParallel, v2, v6)
Convert(matrix2, originParallel, v3, v7)
Convert(matrix2, originParallel, v4, v8)
Convert(matrix2, originParallel, v5, v6)
Convert(matrix2, originParallel, v6, v7)
Convert(matrix2, originParallel, v7, v8)
Convert(matrix2, originParallel, v8, v5)

originIsometric = (1000, 200)  # Midden van kubus
matrix3 = (1 / math.sqrt(2), 0, 1 / math.sqrt(2),
           1 / math.sqrt(6), math.sqrt(2 / 3), -(1 / math.sqrt(6)))

Convert(matrix3, originIsometric, v1, v2)
Convert(matrix3, originIsometric, v2, v3)
Convert(matrix3, originIsometric, v3, v4)
Convert(matrix3, originIsometric, v4, v1)
Convert(matrix3, originIsometric, v1, v5)  #
Convert(matrix3, originIsometric, v2, v6)  #
Convert(matrix3, originIsometric, v3, v7)  #
Convert(matrix3, originIsometric, v4, v8)  #
Convert(matrix3, originIsometric, v5, v6)  #
Convert(matrix3, originIsometric, v6, v7)  #
Convert(matrix3, originIsometric, v7, v8)  #
Convert(matrix3, originIsometric, v8, v5)  #

#Rotatie alle lijnen met 35 graden

v1 = rotate(35, v1)
v2 = rotate(35, v2)
v3 = rotate(35, v3)
v4 = rotate(35, v4)
v5 = rotate(35, v5)
v6 = rotate(35, v6)
v7 = rotate(35, v7)
v8 = rotate(35, v8)
originRotate = (600, 600)

Convert(matrix3, originRotate, v1, v2)
Convert(matrix3, originRotate, v2, v3)
Convert(matrix3, originRotate, v3, v4)
Convert(matrix3, originRotate, v4, v1)
Convert(matrix3, originRotate, v1, v5)
Convert(matrix3, originRotate, v2, v6)
Convert(matrix3, originRotate, v3, v7)
Convert(matrix3, originRotate, v4, v8)
Convert(matrix3, originRotate, v5, v6)
Convert(matrix3, originRotate, v6, v7)
Convert(matrix3, originRotate, v7, v8)
Convert(matrix3, originRotate, v8, v5)

l.draw()
