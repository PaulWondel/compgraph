from lines import *
import numpy as np

l = Lines(640, 480)

#homogene coördinaten
v1 = np.array([-1, -1, -1, 1])
v2 = np.array([1, -1, -1, 1])
v3 = np.array([1, 1, -1, 1])
v4 = np.array([-1, 1, -1, 1])
v5 = np.array([-1, -1, 1, 1])
v6 = np.array([1, -1, 1, 1])
v7 = np.array([1, 1, 1, 1])
v8 = np.array([-1, 1, 1, 1])
matrixCoordinates_ = [v1, v2, v3, v4, v5, v6, v7, v8]


def fuckall(array, angle):
    scale = np.array([[100, 0, 0, 0],
                      [0, 100, 0, 0],
                      [0, 0, 100, 0],
                      [0, 0, 0, 1]])

    rot = np.array([[np.cos(angle), 0, np.sin(angle), 0],
                    [0, 1, 0, 0],
                    [-np.sin(angle), 0, np.cos(angle), 0],
                    [0, 0, 0, 1]])

    trans = np.array([[1, 0, 0, 300],
                     [0, 1, 0, 100],
                     [0, 0, 1, 100],
                     [0, 0, 0, 100]])

    iso = np.array([[(1/np.sqrt(2)), 0, (1/np.sqrt(2)), 0],
                    [(1/np.sqrt(6)), np.sqrt(2/3), -(1/np.sqrt(6)), 0]])
					
    par = np.array([[1, 0, (1/2), 0],
                    [0, 1, (1/2), 0]])

    proj = iso.dot(trans.dot(scale.dot(rot)))
    print(proj)
    for index, p in enumerate(array):
        array[index] = proj.dot(p)

    return array


def drawcube(array):

    l.addLine(abs(array[0]), abs(array[1]))
    l.addLine(abs(array[1]), abs(array[2]))
    l.addLine(abs(array[2]), abs(array[3]))
    l.addLine(abs(array[3]), abs(array[0]))
    l.addLine(abs(array[0]), abs(array[4]))
    l.addLine(abs(array[1]), abs(array[5]))
    l.addLine(abs(array[2]), abs(array[6]))
    l.addLine(abs(array[3]), abs(array[7]))
    l.addLine(abs(array[4]), abs(array[5]))
    l.addLine(abs(array[5]), abs(array[6]))
    l.addLine(abs(array[6]), abs(array[7]))
    l.addLine(abs(array[7]), abs(array[4]))


projectionArray = fuckall(matrixCoordinates_, 30)
drawcube(projectionArray)
l.draw()

